var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// example market and language on url
app.use('/', function (req, res, next) {
  var pathname = req.url.split('&')[0];
  var pathItems = pathname.slice(1).replace(/\/$/, '').split('/');

  var marketAssigned = 'jpa'; // TODO: figure out how to get this value here
  var marketUrlMatch = pathItems.length > 0 ? pathItems[0].match(/[a-z]{3}/) : null;
  var marketUrlValue = marketUrlMatch ? marketUrlMatch[0] : null;

  var languageAssigned = 'es'; // TODO: figure out how to get this value here
  var languageUrlMatch = pathItems.length > 1 ? pathItems[1].match(/[a-z]{2}(-[a-z]{2})?/) : pathItems.length > 0 ? pathItems[0].match(/[a-z]{2}(-[a-z]{2})?/) : null;
  var languageUrlValue = languageUrlMatch ? languageUrlMatch[0] : null;

  var redirectUrl = req.url;

  if (marketUrlValue) {
    redirectUrl = redirectUrl.slice(marketUrlValue.length + 1);
  }

  if (languageUrlValue) {
    redirectUrl = redirectUrl.slice(languageUrlValue.length + 1);
  }

  if (languageAssigned !== 'en') {
    redirectUrl = '/' + languageAssigned + redirectUrl;
  }

  if (marketAssigned !== 'usa') {
    redirectUrl = '/' + marketAssigned + redirectUrl;
  }

  // FIXME: routes like /qwerty/asdfgh don't redirect properly

  if (req.url !== redirectUrl) {
    // Redirect to the proper URL
    res.redirect(redirectUrl);
    //res.redirect('/' + marketUrlValue + '/' + languageUrlValue + req.url);
  } else {
    // If there is no need to redirect just continue with match
    next();
  }
});

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
